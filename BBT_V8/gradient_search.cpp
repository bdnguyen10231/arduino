#include "gradient_search.h"

void gradient_search::set_tolerance(float tolerance){
  gradient_search::tolerance = tolerance;
}

void gradient_search::set_scale(float scale){
  gradient_search::scale = scale;
}

void gradient_search::set_step(float step_size[2]){
  gradient_search::step_size[0] = step_size[0];
  gradient_search::step_size[1] = step_size[1];
}

void gradient_search::set_bds(float bds[2]){
  bool issue = false;
  
  if (bds[0] > bds[1]){
    Serial.println("bound array order incorrect");
    issue = true;
  }

  if (bds[0] < 0 || bds[1] < 0){
    Serial.println("bounds must be positive");
    issue = true;
  }

  if (issue == false){
    bounds[0] = bds[0];
    bounds[1] = bds[1];
  }
}

float gradient_search::get_gradient(){
  return true_gradient;
}

void gradient_search::reset_bounce(){
  count = 0;
}

void gradient_search::initialization(float curr_step, float curr_power){
  step_arr[0] = curr_step;
  power_arr[0] = curr_power*scale;
}

float gradient_search::gradient(float curr_step, float curr_power){
  // sets current values
  
  step_arr[1] = curr_step;
  power_arr[1] = curr_power*scale;

  gradient_value[0] = gradient_value[1];

  // prevent zero division
  if (abs(step_arr[1] - step_arr[0]) < 0.001){
    gradient_value[1] = 0 ;
  }else {
    gradient_value[1] = (power_arr[1] - power_arr[0])/abs(step_arr[1] - step_arr[0]);
  }

  nx_step[0] = (step_arr[1] - step_arr[0])/abs(step_arr[1] - step_arr[0]);

  sign[0] = sign[1];
  sign[1] = gradient_value[1]/abs(gradient_value[1]);

  true_gradient = gradient_value[1];

  // prevent system from bouncing around peak
  if (sign[1] != sign[0])
    count++;

  if (count > 2)
    gradient_value[1] = 0;

  // acceptable tolerance for gradient
  if (abs(true_gradient) <= tolerance)
    gradient_value[1] = 0;

  step_arr[0] = step_arr[1];
  power_arr[0] = power_arr[1];

  if (bounds[1] > power_arr[1] && power_arr[1] > bounds[0]){
    gradient_value[1] = 0; 
  } else if (power_arr[1] < bounds[0]){
    gradient_value[1] = gradient_value[1];
  } else if (power_arr[1] > bounds[1]){
    gradient_value[1] = -1*gradient_value[1];
  }

  if (gradient_value[1] != 0){
    nx_step[1] = sign[1]*nx_step[0];
  
    // set step size 
    if (abs(true_gradient) > 0.5){
      nx_step[1] = nx_step[1]*step_size[0];
    } else{
      nx_step[1] = nx_step[1]*step_size[1];
    }
  
    // detect if bounce
    if (sign[1] < 0){
      if (nx_step[1] > 0){
        nx_step[1] = nx_step[1] + cal[0];
      } else{
        nx_step[1] = nx_step[1] + cal[1];
      }
    }
    
  } else{
    nx_step[1] = 0;
  }

  return nx_step[1];
}

bool gradient_search::check_gradient(){
  if (abs(gradient_value[1]) <= tolerance ) {
    gradient_complete = true;
  } else {
    gradient_complete = false;
  }
  return gradient_complete;
}
