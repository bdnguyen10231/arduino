#include <AccelStepper.h>
#include <MultiStepper.h>

#include "PrintStream.h"
#include "gradient_search.h"

AccelStepper H1(AccelStepper::HALF4WIRE, 5, 6, 7, 8); // real h1
AccelStepper H2(AccelStepper::HALF4WIRE, 14, 15, 16, 17); // real v1
AccelStepper V1(AccelStepper::HALF4WIRE, 9, 10, 11, 12); // real h2
AccelStepper V2(AccelStepper::HALF4WIRE, 3, 2, 1, 0); real v2

gradient_search optim_v2;
gradient_search optim_v1;
gradient_search optim_h2;
gradient_search optim_h1;

const byte numChars = 32;

char config_info[numChars];

int buffer_ind = 0;

typedef struct{
  char action[numChars];
  char motor[numChars];
  float value[2];
} command;

command com;

int motor_speed = 750;
int step_size = 50;

float power = 0;

int avg_sz = 20; // how many values to average
int delay_time = 10; // ms

float noise = 2;
float power_bds[2] = {289, 293};
float step_bds = 1300;

void setup() {
  Serial.begin(115200);

  H1.setMaxSpeed(2000);
  H2.setMaxSpeed(2000);
  V1.setMaxSpeed(2000);
  V2.setMaxSpeed(2000);
}

void loop() {
  if (Serial.available() > 0){
    buffer_ind = 0;
    while (Serial.available() > 0){
      config_info[buffer_ind] = Serial.read();
      buffer_ind++;
    }
    parse_data(config_info);
    check_action();
  }    
}

int calibration(AccelStepper *motor_device, int step_size){
  int start_pose = current_position(motor_device);
  int start_power = powerRead(avg_sz, delay_time);
  float cal_power = powerRead(avg_sz, delay_time);

  if (step_size != 0){
    while (cal_power <= start_power + noise && cal_power >= start_power - noise){
      if (abs(current_position(motor_device)) > step_bds)
        break;
      if (cal_power > power_bds[0] && cal_power < power_bds[1])
        break;
      rel_move(motor_device, step_size);
      cal_power = powerRead(avg_sz, delay_time);
    }
  }
  
  return current_position(motor_device) - start_pose;
}

void tuning(){
  bool v2_complete = false;
  bool v1_complete = false;
  bool h2_complete = false;
  bool h1_complete = false;

  int timer = 0;
  
  int h1_step = 0;
  int h2_step = 0;
  int v1_step = 0;
  int v2_step = 0;

  //while(power < power_bds[0] || power > power_bds[1]){
    
    optim_v2.initialization(current_position(&V2), powerRead(avg_sz, delay_time));
    calibration(&V2, random(-1, 2)*step_size);
  
    optim_v1.initialization(current_position(&V1), powerRead(avg_sz, delay_time));
    calibration(&V1, random(-1, 2)*step_size);
  
    optim_h2.initialization(current_position(&H2), powerRead(avg_sz, delay_time));
    calibration(&H2, random(-1, 2)*step_size);
  
    optim_h1.initialization(current_position(&H1), powerRead(avg_sz, delay_time));
    calibration(&H1, random(-1, 2)*step_size);
    power = powerRead(avg_sz, delay_time); 
    
    while(v2_complete == false){
      while(v1_complete == false){
        while (h2_complete == false){
          while (h1_complete == false){
            timer = millis();
            h1_step = int(optim_h1.gradient(current_position(&H1), power));
            rel_move(&H1, h1_step);         
            power = powerRead(avg_sz, delay_time);
            timer = millis() - timer;
            Serial << "H1" << " , " << optim_h1.get_gradient() << " , " << power << " , " <<  h1_step << " , " << current_position(&H1) << " , " << timer << endl;
      
            if (power > power_bds[0] && power < power_bds[1])
              goto max_value;
      
            h1_complete = optim_h1.check_gradient();
          }
    
          optim_h1.reset_bounce();
          h1_complete = false;

          timer = millis();
          h2_step = int(optim_h2.gradient(current_position(&H2), power));
          rel_move(&H2, h2_step);
          power = powerRead(avg_sz, delay_time);
          timer = millis() - timer;
          Serial << "H2" << " , " << optim_h2.get_gradient() << " , " << power << " , " <<  h2_step << " , " << current_position(&H2) << " , " << timer << endl;
      
          if (power > power_bds[0] && power < power_bds[1])
            goto max_value;
          
          h2_complete = optim_h2.check_gradient();
        }
      
        optim_h2.reset_bounce();
        h2_complete = false;

        timer = millis();
        v1_step = int(optim_v1.gradient(current_position(&V1), power));
        rel_move(&V1, v1_step);
        power = powerRead(avg_sz, delay_time);
        timer = millis() - timer;
        Serial << "V1" << " , " << optim_v1.get_gradient() << " , " << power << " , " <<  v1_step << " , " << current_position(&V1) << " , " << timer << endl;
  
        if (power > power_bds[0] && power < power_bds[1])
          goto max_value;
        
        v1_complete = optim_v1.check_gradient();
      }
      
      optim_v1.reset_bounce();
      v1_complete = false;

      timer = millis();
      v2_step = int(optim_v2.gradient(current_position(&V2), power));
      rel_move(&V2, v2_step);  
      power = powerRead(avg_sz, delay_time);
      timer = millis() - timer;
      Serial << "V2" << " , " << optim_v2.get_gradient() << " , " << power << " , " <<  v2_step << " , " << current_position(&V2) << " , " << timer << endl;
  
      if (power > power_bds[0] && power < power_bds[1])
        goto max_value;
      
      v2_complete = optim_v2.check_gradient();
    }

    optim_v2.reset_bounce();
    v2_complete = false;
 // }
  
  max_value: // break out of nested loop
  Serial.println("Tuning Done");
}


void max_velocity(AccelStepper *motor_device, float vel){
  motor_device -> setMaxSpeed(int(vel));
}

void set_velocity(float vel){
  motor_speed = vel;
}

bool rel_move(AccelStepper *motor_device, float pos){
  motor_device -> move(pos);
  motor_device -> setSpeed(int(motor_speed));
  while(motor_device -> distanceToGo() != 0){
    if (abs(current_position(motor_device)) > step_bds ){
      // bound motors to 2000 half-steps around 180 degrees
      break;
    }
    motor_device -> runSpeedToPosition();
  }
  return true;
}

bool abs_move(AccelStepper *motor_device, float pos){
  motor_device -> moveTo(pos);
  motor_device -> setSpeed(int(motor_speed));
  while(motor_device -> distanceToGo() != 0){
    motor_device -> runSpeedToPosition();
  }
  return true;
}

void set_tolerance(float tolerance){
  Serial.print("Learning Rate: ");
  Serial.println(tolerance);
  optim_h1.set_tolerance(tolerance);
  optim_h2.set_tolerance(tolerance);
  optim_v1.set_tolerance(tolerance);
  optim_v2.set_tolerance(tolerance);
}

void set_step(float steps[2]){
  optim_h1.set_step(steps);
  optim_h2.set_step(steps);
  optim_v1.set_step(steps);
  optim_v2.set_step(steps);
}

void set_x_scale(float x){
  Serial.print("X Scale: (Doesn't Work!)");
  Serial.println(x);
}

void set_y_scale(float y){
  optim_h1.set_scale(y);
  optim_h2.set_scale(y);
  optim_v1.set_scale(y);
  optim_v2.set_scale(y);
}

void motor_off(AccelStepper *motor_device){
  motor_device -> disableOutputs();
}

void motor_on(AccelStepper *motor_device){
  motor_device -> enableOutputs();
}

int current_position(AccelStepper *motor_device){
  return motor_device -> currentPosition();
}

float powerRead(int sz, float dtime){
  float avg = 0;
  for(int i = 0; i < sz; i++){
    delay(dtime);
    avg += analogRead(23);
  }
  return avg/sz;
  
}

float calc_noise_gaus(int sz){
  // This noise calculation only works if noise distribution is gaussian
  float mean = 0;
  float std = 0;
  
  for (int i = 0; i < sz; i++){
    mean = mean + powerRead(avg_sz, delay_time); // NOT MEAN need to divide by number of samples 
    if (i > 1){
      std = std + pow(powerRead(avg_sz, delay_time)-mean/i, 2);
      Serial.println(sqrt(std/i));
      Serial.println(mean/i);
    }
  }
  
  return 3*sqrt(std/sz);
}

void parse_data(char* msg){
  char *token;
  const char* s = " ";
  
  token = strtok(msg, s);
  strcpy(com.motor, token);
  
  token = strtok(NULL, s);
  strcpy(com.action, token);

  token = strtok(NULL, s);
  com.value[0] = atof(token);

  token = strtok(NULL, s);
  com.value[1] = atof(token);
}

AccelStepper* check_motor(){
  if (strcmp(com.motor, "H1") == 0){
    return &H1;
  } else if (strcmp(com.motor, "H2") == 0){
    return &H2;
  } else if (strcmp(com.motor, "V1") == 0){
    return &V1;
  } else if (strcmp(com.motor, "V2") == 0){
    return &V2;
  } else {
    Serial.println("Did not provid valid motor type");
  }
}

void check_action(){
  if(strcmp(com.action, "RM") == 0){ 
    Serial.println(rel_move(check_motor(), com.value[0]));
  }else if(strcmp(com.action, "AM") == 0){
    Serial.println(abs_move(check_motor(), com.value[0]));
  }else if(strcmp(com.action, "MV") == 0){
    max_velocity(check_motor(), com.value[0]);
  }else if(strcmp(com.action, "SV") == 0){
    set_velocity(com.value[0]);
  }else if(strcmp(com.action, "SS") == 0){
    set_step(com.value);
  }else if(strcmp(com.action, "SX") == 0){
    set_x_scale(com.value[0]);
  }else if(strcmp(com.action, "SY") == 0){
    set_y_scale(com.value[0]);
  }else if(strcmp(com.action, "ST") == 0){
    set_tolerance(com.value[0]);
  }else if(strcmp(com.action, "CP") == 0){
    Serial.println(current_position(check_motor()));
  }else if(strcmp(com.action, "GP") == 0){
    Serial.println(powerRead(avg_sz, delay_time));
  }else if(strcmp(com.action, "T") == 0){
    tuning();
  }else if(strcmp(com.action, "ON") == 0){
    motor_on(&H1);
    motor_on(&H2);
    motor_on(&V1);
    motor_on(&V2);
  }else if(strcmp(com.action, "OFF") == 0){
    motor_off(&H1);
    motor_off(&H2);
    motor_off(&V1);
    motor_off(&V2);
  }else if(strcmp(com.action, "CAL") == 0){
    do {
      calibration(check_motor(), -com.value[0]);
      delay(500);
      optim_h1.cal[0] = calibration(&H1, com.value[0]);
      delay(500);
      optim_h1.cal[1] = calibration(&H1, -com.value[0]);
      delay(500);
    } while (abs(optim_h1.cal[0]) != abs(optim_h1.cal[1]));
    
    do {
      calibration(check_motor(), -com.value[0]);
      delay(500);
      optim_h2.cal[0] = calibration(&H2, com.value[0]);
      delay(500);
      optim_h2.cal[1] = calibration(&H2, -com.value[0]);
      delay(500);
    } while (abs(optim_h2.cal[0]) != abs(optim_h2.cal[1]));

    do {
      calibration(check_motor(), -com.value[0]);
      delay(500);
      optim_v1.cal[0] = calibration(&V1, com.value[0]);
      delay(500);
      optim_v1.cal[1] = calibration(&V1, -com.value[0]);
      delay(500);
    } while (abs(optim_v1.cal[0]) != abs(optim_v1.cal[1]));
    
    do {
      calibration(check_motor(), -com.value[0]);
      delay(500);
      optim_v2.cal[0] = calibration(&V2, com.value[0]);
      delay(500);
      optim_v2.cal[1] = calibration(&V2, -com.value[0]);
      delay(500);
    } while (abs(optim_v2.cal[0]) != abs(optim_v2.cal[1]));
    
    Serial.println("Calibration Complete");
  }else if(strcmp(com.action, "GB") == 0){
    optim_h1.set_bds(com.value);
    optim_h2.set_bds(com.value);
    optim_v1.set_bds(com.value);
    optim_v2.set_bds(com.value);
  }else if(strcmp(com.action, "PB") == 0){
    power_bds[0] = com.value[0];
    power_bds[1] = com.value[1];
  }else if(strcmp(com.action, "SB") == 0){
    step_bds = com.value[0];
  }else if(strcmp(com.action, "N") == 0){
    noise = com.value[0];
  }else if(strcmp(com.action, "PF") == 0){
    avg_sz = com.value[0];
    delay_time = com.value[1];
  }
}
