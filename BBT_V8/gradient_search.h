#include <Arduino.h>

#ifndef GRADIENT_SEARCH_H
#define GRADIENT_SEARCH_H

//#define debug2

class gradient_search{
  public:
    void set_tolerance(float tolerance);
    void set_scale(float scale);
    void set_step(float step_size[2]);
    void set_bds(float bds[2]);
    float get_gradient();
    void initialization(float curr_step, float curr_power);
    float gradient(float curr_step, float curr_power);
    bool check_gradient();
    void reset_bounce();

    int cal[2] = {150, -150}; // {CW, CCW}
    float step_size[2] = {50, 15}; // {big step, small step}


  private:    
    int sign[2]; // { n-1, n} this is sign of gradients. different from sign of step
    int count = 0;
    float bounds[2] = {289, 294};  // { lower bound, upper bound }
    float step_arr[2]; // { previous step, current step}
    float nx_step[2]; // {sign of previous step, step motor should take}
    float power_arr[2]; // {n-1, n}
    float gradient_value[2]; // {n-1, n}
    float tolerance = 0.01;
    float scale = 1;
    float true_gradient = 0.0;
    bool gradient_complete = false;
  };

#endif
